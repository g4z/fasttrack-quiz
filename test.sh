
# script to simulate a bunch of sequential requests to create some data

for i in $(seq 1 50)
do
	a=$(((RANDOM%3)+1))
	b=$(((RANDOM%3)+1))
	c=$(((RANDOM%3)+1))
	echo -e "$a\n$b\n$c\n" | go run main.go | sed '/^$/d' | grep -E 'Score:|You scored'
done
