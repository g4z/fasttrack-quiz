package main

import (
	"encoding/json"
	"net/http"
	"sync"

	"github.com/google/uuid"
)

var (
	quizData *QuizData
	sessions *Sessions
)

type QuizData struct {
	Questions []Question
}

func (q *QuizData) GetQuestion(id string) *Question {
	for _, question := range q.Questions {
		if question.ID == id {
			return &question
		}
	}
	return nil
}

type Answer struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

type Question struct {
	ID            string   `json:"id"`
	Text          string   `json:"text"`
	Answers       []Answer `json:"answers"`
	CorrectAnswer string   `json:"-"`
}

type Session struct {
	ID      string
	Answers map[string]string
}

type Sessions struct {
	sync.Mutex
	Sessions map[string]*Session
}

func (s *Sessions) Get(sessionId string) *Session {
	s.Lock()
	defer s.Unlock()
	return s.Sessions[sessionId]
}

func (s *Sessions) Create(sessionId string) *Session {
	session := &Session{
		ID:      sessionId,
		Answers: make(map[string]string),
	}
	s.Lock()
	s.Sessions[sessionId] = session
	s.Unlock()
	return session
}

func init() {

	sessions = &Sessions{
		Sessions: make(map[string]*Session),
	}

	LoadQuizData()
}

// load quiz data from storage
func LoadQuizData() {
	quizData = &QuizData{
		Questions: []Question{
			{
				ID:   "da6ad6d9-bc34-4c26-bd53-18f610e8c76a",
				Text: "What Planet is closest to the sun?",
				Answers: []Answer{{
					ID:   "9ef63e34-1f4c-405d-8c41-3aca0c3b4537",
					Text: "Earth",
				}, {
					ID:   "534fa688-e7ab-4ce5-b426-3bf869f19426",
					Text: "Mercury",
				}, {
					ID:   "4f2a615a-539b-46d5-877b-64e6c8764915",
					Text: "Jupiter",
				}},
				CorrectAnswer: "534fa688-e7ab-4ce5-b426-3bf869f19426",
			},
			{
				ID:   "0ea7ea07-edf3-446a-86aa-2ad0ffa6972e",
				Text: "Which of these planets is the largest?",
				Answers: []Answer{{
					ID:   "8749ec53-0568-40e5-ba0e-53e4127e8e07",
					Text: "Jupiter",
				}, {
					ID:   "ecb7f58e-0395-4cd1-86f8-1c55924517b3",
					Text: "Saturn",
				}, {
					ID:   "9c5304ba-7762-4115-9067-357b6eacf6a0",
					Text: "Neptune",
				}},
				CorrectAnswer: "8749ec53-0568-40e5-ba0e-53e4127e8e07",
			},
			{
				ID:   "02f6098a-1974-43b5-ac5e-ca3b7b0fb546",
				Text: "What Planet is closest to earth?",
				Answers: []Answer{{
					ID:   "dd6b7829-2f43-4bd5-b962-0c938d1596c3",
					Text: "Jupiter",
				}, {
					ID:   "a290b80c-1d7d-42d7-9830-d89d5b37c43a",
					Text: "Moon",
				}, {
					ID:   "58cd57ba-f80f-4c8a-93e2-7d05faa686dd",
					Text: "Mars",
				}},
				CorrectAnswer: "58cd57ba-f80f-4c8a-93e2-7d05faa686dd",
			},
		},
	}
}

// the client should call GetQuestion endpoint until status
// changes from "ok" to "complete"
func GetQuestion(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	// check if session cookie was sent
	// if not, it's the 1st question, set cookie
	var sessionId string
	cookie, err := r.Cookie("quiz-cookie")
	if err == nil {
		sessionId = cookie.Value
	} else {
		sessionId = uuid.New().String()
		w.Header().Set("quiz-cookie", sessionId)
	}

	// find or create session
	session := sessions.Get(sessionId)
	if session == nil {
		session = sessions.Create(sessionId)
	}

	// compute the current question index
	qindex := len(session.Answers)

	// if all questions are complete, return results
	if qindex >= len(quizData.Questions) {
		json.NewEncoder(w).Encode(struct {
			Status          string `json:"status"`
			TotalQuestions  int    `json:"total"`
			TotalCorrect    int    `json:"correct"`
			TotalPercentage int    `json:"percentage"`
		}{
			"complete",
			len(quizData.Questions),
			ComputeTotalCorrect(session),
			ComputePercentage(session),
		})

		return
	}

	// get the current question
	data := quizData.Questions[qindex]

	// send response
	json.NewEncoder(w).Encode(struct {
		Status         string   `json:"status"`
		TotalQuestions int      `json:"total"`
		TotalCorrect   int      `json:"correct"`
		ID             string   `json:"id"`
		Text           string   `json:"text"`
		Answers        []Answer `json:"answers"`
	}{
		"ok",
		len(quizData.Questions),
		ComputeTotalCorrect(session),
		data.ID,
		data.Text,
		data.Answers,
	})
}

// client calls this with GET query params
func AnswerQuestion(w http.ResponseWriter, req *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	cookie, err := req.Cookie("quiz-cookie")
	if err != nil {
		RespondFail(w, "No session")
		return
	}

	session := sessions.Get(cookie.Value)
	if session == nil {
		RespondFail(w, "Invalid session")
		return
	}

	// get the requested question
	questionId := req.URL.Query().Get("question")
	if questionId == "" {
		RespondFail(w, "Invalid questionID")
		return
	}

	question := quizData.GetQuestion(questionId)
	if question == nil {
		RespondFail(w, "Invalid question")
		return
	}

	answerId := req.URL.Query().Get("answer")
	if answerId == "" {
		RespondFail(w, "Invalid answerID")
		return
	}

	// don't think this needs a mutex :/
	session.Answers[questionId] = answerId

	json.NewEncoder(w).Encode(struct {
		Status string `json:"status"`
	}{"ok"})
}

// general "fail" response for bad request
func RespondFail(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{"fail", message})
}

func ComputeTotalCorrect(session *Session) int {
	var total = 0
	for questionID, answerID := range session.Answers {
		question := quizData.GetQuestion(questionID)
		if answerID == question.CorrectAnswer {
			total++
		}
	}
	return total
}

func ComputePercentage(session *Session) int {

	var countOfScoredLess = 0
	var totalNumOtherSessions = len(sessions.Sessions) - 1
	var myTotal = ComputeTotalCorrect(session)

	if totalNumOtherSessions < 1 {
		return 100
	}

	for _, s := range sessions.Sessions {
		if s.ID != session.ID {
			if ComputeTotalCorrect(s) < myTotal {
				countOfScoredLess++
			}
		}
	}

	return int((float64(countOfScoredLess) / float64(totalNumOtherSessions)) * 100.0)
}

func main() {

	http.HandleFunc("/GetQuestion", GetQuestion)
	http.HandleFunc("/AnswerQuestion", AnswerQuestion)

	http.ListenAndServe(":8888", nil)
}
