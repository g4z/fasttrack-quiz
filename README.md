
# fasttrack-quiz

## Run the server

```bash
go run server/main.go
```

## Run the client

```bash
go run main.go
```

## Example output

```bash
user@ubuntu:~/code/fasttrack-quiz$ go run main.go

What Planet is closest to the sun?

1. Earth
2. Mercury
3. Jupiter

Choose: 2

Which of these planets is the largest?

1. Jupiter
2. Saturn
3. Neptune

Choose: 3

What Planet is closest to earth?

1. Jupiter
2. Moon
3. Mars

Choose: 3

Score: 2\3

You scored higher than 76% of all quizzers
```
