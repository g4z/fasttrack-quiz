package cmd

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

var (
	sessionId string = ""
	stdin     *bufio.Reader
)

var RootCmd = &cobra.Command{
	Use: "start-quiz",
	Run: func(cmd *cobra.Command, args []string) {

		for {

			println()

			question := GetNextQuestion()

			if question == nil {
				break
			}

			answer := ProcessInput(question)

			PostAnswer(question, answer)
		}
	},
}

type Answer struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

type Question struct {
	Status          string   `json:"status"`
	TotalQuestions  int      `json:"total"`
	CurrentQuestion int      `json:"current"`
	TotalCorrect    int      `json:"correct"`
	TotalPercentage int      `json:"percentage"`
	ID              string   `json:"id"`
	Text            string   `json:"text"`
	Answers         []Answer `json:"answers"`
}

func init() {
	stdin = bufio.NewReader(os.Stdin)
}

func GetNextQuestion() *Question {

	req, err := http.NewRequest("GET", "http://127.0.0.1:8888/GetQuestion", nil)
	if err != nil {
		panic(err)
	}

	if sessionId != "" {
		req.AddCookie(&http.Cookie{Name: "quiz-cookie", Value: sessionId})
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	if resp.Header.Get("quiz-cookie") != "" {
		sessionId = resp.Header.Get("quiz-cookie")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	// println(string(body))

	question := &Question{}
	err = json.Unmarshal(body, question)
	if err != nil {
		panic(err)
	}

	// if the quiz questions are complete
	if question.Status == "complete" {
		fmt.Printf("Score: %d\\%d\n\n", question.TotalCorrect, question.TotalQuestions)
		fmt.Printf("You scored higher than %d%% of all quizzers\n\n", question.TotalPercentage)
		return nil
	}

	fmt.Printf("%s\n\n", question.Text)
	for i, answer := range question.Answers {
		fmt.Printf("%d. %s\n", i+1, answer.Text)
	}

	return question
}

func ProcessInput(question *Question) string {

	var total = len(question.Answers)

	for {
		fmt.Print("\nChoose: ")

		var i int
		_, err := fmt.Fscan(stdin, &i)
		if err == nil {
			i--
			if i >= 0 && i < total {
				return question.Answers[i].ID
			}
		}

		stdin.ReadString('\n')
		fmt.Printf("\n*** Enter a number between 1 and %d ***", total)
	}
}

func PostAnswer(question *Question, answer string) {

	req, err := http.NewRequest("GET", "http://127.0.0.1:8888/AnswerQuestion", nil)
	if err != nil {
		panic(err)
	}

	req.AddCookie(&http.Cookie{Name: "quiz-cookie", Value: sessionId})

	q := req.URL.Query()
	q.Add("question", question.ID)
	q.Add("answer", answer)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		fmt.Printf("\n*** Oops. Need better error client handling ***\n")
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
